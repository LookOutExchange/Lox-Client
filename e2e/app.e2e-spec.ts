import { UnknownPage } from './app.po';

describe('unknown App', () => {
  let page: UnknownPage;

  beforeEach(() => {
    page = new UnknownPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
