import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OrderBoxService} from './order-box.service';
import {OrderItem} from './order-item';

@Component({
  selector: 'app-order-box',
  templateUrl: './order-box.component.html',
  styleUrls: ['./order-box.component.css']
})
export class OrderBoxComponent implements OnInit {
  @Input() showBlock: boolean;
  @Input() orderBy: OrderItem;
  @Output() orderByChange = new EventEmitter<OrderItem>();
  @Output() directionChange = new EventEmitter();

  buttonClass: string;
  classes: string[];
  orderItems: OrderItem[];

  constructor() {
    this.classes = ['closed', 'opened'];
    this.orderItems = OrderBoxService.orderItems;
  }

  ngOnInit() {
    this.setDropdownOpen();
    OrderBoxService.setActive(this.orderBy);
  }

  setDropdownOpen() {
    this.buttonClass = this.classes[Number(this.showBlock)]
  }

  setOrder(item: OrderItem) {
    this.orderBy = item;
    this.orderByChange.emit(this.orderBy);
    OrderBoxService.setActive(item);
  }

  setDirection(ascending: boolean) {
    OrderBoxService.setDirection(ascending);
    this.directionChange.emit();
  }

  getDirection(): boolean {
    return OrderBoxService.orderDirection;
  }
}
