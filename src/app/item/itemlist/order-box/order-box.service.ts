import {Injectable} from '@angular/core';
import {OrderItem} from './order-item';

@Injectable()
export class OrderBoxService {
  // true = ascending, false = descending
  static orderDirection = true;
  static orderItems: OrderItem[] = [
    {name: 'Название', value: 'title'},
    {name: 'Дата размещения', value: 'createdDate'},
    {name: 'Стоимость', value: 'price'}
  ];

  static setDirection(asc: boolean) {
    this.orderDirection = asc;
  }

  static get direction(): string {
    return OrderBoxService.orderDirection ? 'asc' : 'desc';
  }

  static setActive(item: OrderItem) {
    OrderBoxService.clearActiveStatus();
    const index = OrderBoxService.orderItems.findIndex(i => i === item);
    OrderBoxService.orderItems[index].isActive = true;
  }

  static clearActiveStatus() {
    OrderBoxService.orderItems.forEach(i => i.isActive = false);
  }

}
