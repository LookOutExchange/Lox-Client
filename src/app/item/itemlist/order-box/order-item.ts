export interface OrderItem {
  name: string;
  value: string;
  isActive?: boolean;
}
