import {inject, TestBed} from '@angular/core/testing';

import {OrderBoxService} from './order-box.service';

describe('OrderBoxService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrderBoxService]
    });
  });

  it('should be created', inject([OrderBoxService], (service: OrderBoxService) => {
    expect(service).toBeTruthy();
  }));
});
