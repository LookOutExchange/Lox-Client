import {RouteService} from '../../routes/route.service';
import {Category, Item} from '../item';
import {ItemService} from '../item.service';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {OrderItem} from './order-box/order-item';
import {OrderBoxService} from './order-box/order-box.service';
import {User} from '../../user/user';
import {UserService} from '../../user/user.service';


@Component({
  selector: 'app-itemlist',
  templateUrl: './itemlist.component.html',
  styleUrls: ['./itemlist.component.css']
})
export class ItemlistComponent implements OnInit {
  categories: Category[];
  items: Item[];
  header: string;
  view: string;
  page: number;
  totalPages: number;
  itemsOnPage: number;

  user?: User;

  orderBy: OrderItem;
  orderBoxClass: string;

  constructor(private itemService: ItemService, private pageService: RouteService,
              private route: ActivatedRoute, private userService: UserService) {
    this.orderBy = OrderBoxService.orderItems[0];
    this.totalPages = 2;
    this.itemsOnPage = 10;

    userService.userLogged$.subscribe(user => this.user = user);
  }

  ngOnInit() {
    this.itemService.getCategories().subscribe(cat => {
      this.categories = cat;
      this.route.paramMap.subscribe(map => this.parseMap(map));
    });
  }

  private parseMap(map: ParamMap) {
    try {
      this.view = map.get('view');
      this.page = Number(map.get('page'));
    } catch (e) {
    } finally {
      this.page = this.page == null ? 1 : this.page;
      this.view = this.view == null ? 'new' : this.view;
      this.chooseHeader();
    }
  }

  private chooseHeader() {
    switch (this.view) {
      case 'new': {
        this.orderBoxClass = 'hide';
        this.header = 'Новые объявления';
        this.itemService.getItems(this.page, 'createdDate').subscribe(data => this.items = data);
        break;
      }
      case 'popular': {
        this.orderBoxClass = 'hide';
        this.header = 'Популярные объявления';
        this.itemService.getItems(this.page, this.orderBy.value).subscribe(data => this.items = data);
        break;
      }
      case 'own': {
        this.header = 'Ваши объявления';
        this.itemService.getItems(this.page, this.orderBy.value).subscribe(data => this.items = data);
        break;
      }
      default: {
        this.getListByCategory();
      }
    }
  }

  findCategory(): Category {
    return this.categories.find(value => this.view === value.value);
  }

  getListByCategory() {
    let cat = this.findCategory();
    this.header = cat.title;
    this.itemService.getItems(this.page, this.orderBy.value, OrderBoxService.direction, this.view)
      .subscribe(data => this.items = data);
    this.orderBoxClass = 'show';
  }

  handleOrderChange(item: OrderItem) {
    this.orderBy.value = item.value;
    this.getListByCategory();
  }

  handleDirectionChange() {
    this.getListByCategory();
  }

  onPageChange(event: number) {
    this.page = event;
    this.itemService.getItems(this.page, this.orderBy.value, OrderBoxService.direction, this.view);
  }
}
