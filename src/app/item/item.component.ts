import {Component, OnInit} from '@angular/core';
import {Item} from './item';
import {ItemService} from './item.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
})
export class ItemComponent implements OnInit {
  showFeatures: boolean;
  item: Item;
  id: any;

  constructor(private itemService: ItemService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.showFeatures = false;
    this.route.params.subscribe(param => {
      this.id = param['id'];
      this.itemService.getItem(this.id).subscribe(data => this.item = data[this.id - 1]);
    });
  }
}
