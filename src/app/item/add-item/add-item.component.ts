import {Component, OnInit} from '@angular/core';
import {Category} from '../item';
import {ItemService} from '../item.service';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {
  categories: Category[];
  submitted: boolean;

  constructor(private itemService: ItemService) {
    this.submitted = false;
  }

  ngOnInit() {
    this.itemService.getCategories().subscribe(data => this.categories = data)
  }

  onSubmit() {
    this.submitted = true;
  }

}
