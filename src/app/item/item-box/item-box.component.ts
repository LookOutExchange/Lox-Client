import {Component, Input, OnInit} from '@angular/core';
import {Item} from '../item';
import {User} from '../../user/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-item-box',
  templateUrl: './item-box.component.html',
  styleUrls: ['./item-box.component.css']
})
export class ItemBoxComponent implements OnInit {
  @Input() item: Item;
  @Input() user?: User;

  itemLink: string;
  titleLength: number;

  constructor(private  router: Router) {
  }

  ngOnInit() {
    this.itemLink = '/item/';
    this.titleLength = 23;
  }

  redirectToItem() {
    this.router.navigate([this.itemLink + this.item.id]).catch();
  }

  redirectToEdit() {
    this.router.navigate(['/']).catch();
  }
}
