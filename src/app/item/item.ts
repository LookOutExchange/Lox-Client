import {User} from '../user/user';

export interface Item {
  id: number;
  title: string;
  price: number;
  address?: string;
  status?: boolean;
  createdDate?: Date;
  photo?: string;
  description?: string;
  views?: number;
  category?: Category;
  user?: User;
  features?: Feature[];
}

export interface Feature {
  id: number;
  title: string;
  value: string;
}

export interface Category {
  id: number;
  title: string;
  value: string;
}

export interface Date {
  month?: string;
  year?: number;
  dayOfMonth?: number;
  dayOfWeek?: string;
  dayOfYear?: number;
  hour?: number;
  minute?: number;
  nano?: number;
  second?: number;
  monthValue?: number;
  chronology?: Chronology;
}

export interface Chronology {
  id?: string;
  calendarType?: string;
}
