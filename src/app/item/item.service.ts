import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Category, Item} from './item';
import {Observable} from 'rxjs';
import {HttpService} from '../global/http.service';


@Injectable()
export class ItemService extends HttpService {
  newestPath: string;
  retrievePath: string;
  itemPath: string;
  retrieveByCategoryPath: string;
  categoryPath: string;

  constructor(protected http: Http) {
    super(http);
    if (!this.test) {
      this.retrievePath = 'api/item/retrieve/';
      this.retrieveByCategoryPath = 'api/category/retrieve/';
      this.itemPath = 'api/item/retrieveOne/';
      this.categoryPath = 'api/item/category/retrieve';
      this.newestPath = 'api/item/?'
    } else {
      this.retrievePath = 'assets/items.json';
      this.retrieveByCategoryPath = 'assets/items.json';
      this.itemPath = 'assets/items.json';
      this.categoryPath = 'assets/categories';
      this.newestPath = 'assets/items.json'
    }
  }

  getItems(page: number, order?: string, orderDirection?: string, category?: string): Observable<Item[]> {
    const path = this.test ? this.retrievePath : this.retrievePath.concat(String(page));
    const params = this.setParams([{k: 'orderBy', v: order}, {k: 'orderDirection', v: orderDirection},
      {k: 'categoryValue', v: category}]);
    return this.http
      .get(path, params)
      .map(this.extractData).catch(this.handleError);
  }

  getLastItems(): Observable<Item[]> {

    return this.http
      .get(this.retrievePath)
      .map(this.extractData).catch(this.handleError);
  }

  getItemsByCategory(category: string): Observable<Item[]> {
    const param = [{k: 'category', v: category}];
    return this.http
      .get(this.retrievePath, this.setParams(param))
      .map(this.extractData).catch(this.handleError);
  }

  getItem(id: string): Observable<Item> {
    const path = this.test ? this.itemPath : this.itemPath.concat(id);
    return this.http
      .get(path)
      .map(this.extractData).catch(this.handleError);
  }

  getCategories(): Observable<Category[]> {
    return this.http
      .get(this.categoryPath)
      .map(this.extractData).catch(this.handleError);
  }

}

