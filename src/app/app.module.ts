import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RootComponent} from './root.component';
import {ItemComponent} from './item/item.component';
import {ItemService} from './item/item.service';
import {WelcomeComponent} from './welcome/welcome.component';
import {RouterModule, Routes} from '@angular/router';
import {ItemlistComponent} from './item/itemlist/itemlist.component';
import {AboutComponent} from './about/about.component';
import {ContactsComponent} from './contacts/contacts.component';
import {PhotoComponent} from './photo/photo.component';
import {ItemBoxComponent} from './item/item-box/item-box.component';
import {AddItemComponent} from './item/add-item/add-item.component';
import {RouteService} from './routes/route.service';
import {RegistrationComponent} from './user/registration/registration.component';
import {OrderBoxComponent} from './item/itemlist/order-box/order-box.component';
import {OrderBoxService} from './item/itemlist/order-box/order-box.service';
import {UserService} from './user/user.service';
import {LoginComponent} from './user/login/login.component';
import { UserBarComponent } from './user/user-bar/user-bar.component';
import {NgxPaginationModule, PaginationControlsComponent, PaginationControlsDirective} from 'ngx-pagination';

const appRoutes: Routes = [
  {path: '', component: WelcomeComponent, pathMatch: 'full'},
  {path: 'item/retrieve', component: ItemlistComponent},
  {path: 'item/add', component: AddItemComponent, pathMatch: 'full'},
  {path: 'about', component: AboutComponent},
  {path: 'contacts', component: ContactsComponent},
  {path: 'item/:id', component: ItemComponent, pathMatch: 'full'},
  {path: 'items/:view/:page', component: ItemlistComponent},
  {path: 'items/:view', component: ItemlistComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: 'login', component: LoginComponent}
];

@NgModule({
  declarations: [
    RootComponent,
    ItemComponent,
    WelcomeComponent,
    ItemlistComponent,
    AboutComponent,
    ContactsComponent,
    ItemBoxComponent,
    PhotoComponent,
    AddItemComponent,
    RegistrationComponent,
    OrderBoxComponent,
    LoginComponent,
    UserBarComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  providers: [ItemService, RouteService, OrderBoxService, UserService],
  bootstrap: [RootComponent]
})

export class AppModule {
}
