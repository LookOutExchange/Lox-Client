import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnInit {
  @Input() width?: string;
  @Input() height?: string;
  @Input() source?: string;
  @Input() className?: string;

  constructor() {
  }


  ngOnInit() {
  }


  errorHandle(event) {
    event.target.error = null;
    event.target.src = 'assets/images/product/no-image.png';
  }

}
