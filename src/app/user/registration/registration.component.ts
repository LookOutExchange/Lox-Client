import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators} from '@angular/forms';
import {UserService} from '../user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: RegistrationComponent,
    multi: true
  },
    {
      provide: NG_VALIDATORS,
      useExisting: RegistrationComponent,
      multi: true
    }
  ]
})
export class RegistrationComponent implements OnInit {
  firstName: string;
  secondName: string;
  email: string;
  password: string;
  password2: string;

  form: FormGroup;

  submitTouched: boolean;

  constructor(private userService: UserService) {
    this.userService.logInEvent(null);
  }

  ngOnInit() {
    this.submitTouched = false;

    const passPattern = '^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$';
    const mailPattern = '^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)' +
      '|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)' +
      '+[a-zA-Z]{2,}))$';

    this.form = new FormGroup({
      'firstName': new FormControl(this.firstName, [Validators.required]),
      'secondName': new FormControl(this.secondName, [Validators.required]),
      'email': new FormControl(this.email, [Validators.required, Validators.pattern(mailPattern)]),
      'password': new FormControl(this.password, [Validators.minLength(6), Validators.pattern(passPattern)]),
    });
  }

  get firstNameC() {
    return this.form.get('firstName');
  }

  get secondNameC() {
    return this.form.get('secondName');
  }

  get emailC() {
    return this.form.get('email');
  }

  get passwordC() {
    return this.form.get('password');
  }

  onSubmit() {
    this.submitTouched = true;
    console.log(this.form.valid);
    if (this.form.valid && this.password === this.password2) {
    this.userService.createUser(this.firstName, this.secondName, this.email, this.password).subscribe();
    }
  }

}
