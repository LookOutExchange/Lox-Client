import {EventEmitter, Injectable} from '@angular/core';
import {User} from './user';
import {Http} from '@angular/http';
import {HttpService} from '../global/http.service';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class UserService extends HttpService {

  // user: User;
  loginPath: string;
  createPath = '/api/user/signUp';
  loginObserve = new Subject<User>();
  userLogged$ = this.loginObserve.asObservable();


  constructor(protected http: Http) {
    super(http);
    if (this.test) {
      this.loginPath = 'assets/users';
    } else {
      this.loginPath = 'api/user/login';
    }
  }

  createUser(firstName: string, secondName: string, email: string, password: string): Observable<boolean> {
    const param = [{k: 'firstName', v: firstName}, {k: 'secondName', v: secondName},
      {k: 'email', v: email}, {k: 'password', v: password}];
    return this.http.post(this.createPath, this.setParams(param)).map(this.extractData).catch(this.handleError);
  }

  login(email: string, password: string) {
    const param = [{k: 'email', v: email}, {k: 'password', v: password}];
    if (this.test) {
      return this.http.get(this.loginPath, this.setParams(param)).map(this.extractData).catch(this.handleError);
    } else {
      return this.http.post(this.loginPath, this.setParams(param)).map(this.extractData).catch(this.handleError);
    }
  }

  logInEvent(user: User) {
    this.setToLocalStorage(user);
    this.loginObserve.next(user);
  }

  setToLocalStorage(usr: User) {
    this.setToStorage(localStorage, usr);
  }

  getFromLocalStorage(): User {
    return this.getFromStorage(localStorage);
  }

  private getFromStorage(storage: Storage) {
    const retrievedUserJSON = storage.getItem('user');
    try {
      return JSON.parse(retrievedUserJSON);
    } catch (e) {
      return null;
    }
  }

  private setToStorage(storage: Storage, usr: User) {
    storage.setItem('user', JSON.stringify(usr));
  }

}
