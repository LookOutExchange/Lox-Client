import {Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {Router} from '@angular/router';
import {getComponent} from '@angular/core/src/linker/component_factory_resolver';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../registration/registration.component.css']
})
export class LoginComponent implements OnInit {

  email: string;
  password: string;

  constructor(private userService: UserService, private router: Router) {
    this.userService.logInEvent(null);
  }

  ngOnInit() {
  }

  onSubmit() {
    this.userService.login(this.email, this.password).subscribe(user => {
      this.userService.logInEvent(user);
      this.userService.setToLocalStorage(user);
      this.router.navigate(['/']).catch();
    }, err => {
    });

  }
}
