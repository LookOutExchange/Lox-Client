export interface User {
  id: number;
  firstName: string;
  secondName: string;
  email: string;
  password?: string;
  status?: boolean;
  role: Role;
  avatar?: string;
}

export interface Role {
  id: number;
  title: string;
}
