import {Component, Input, OnInit} from '@angular/core';
import {User} from '../user';
import {UserService} from '../user.service';

@Component({
  selector: 'app-user-bar',
  templateUrl: './user-bar.component.html',
  styleUrls: ['./user-bar.component.css']
})
export class UserBarComponent implements OnInit {
  @Input() user?: User;
  @Input() opened: boolean;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
  }

  logOut = function () {
    this.userService.logInEvent(null);
  }
}
