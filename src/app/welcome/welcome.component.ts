import {Item} from '../item/item';
import {ItemService} from '../item/item.service';
import {Component, OnInit} from '@angular/core';
import {RouteService} from '../routes/route.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  items: Item[];
  newItemLink: string;
  popularItemLink: string;


  constructor(private itemService: ItemService, pageService: RouteService) {
    this.newItemLink = pageService.getNewItemsPage();
    this.popularItemLink = pageService.getPopularItemsPage();
  }

  ngOnInit() {
    this.itemService.getItems(1).subscribe(data => this.items = data);
  }
}
