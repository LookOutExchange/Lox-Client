import {Page, Restriction} from './iroute';

const leftClass = 'left';
const rightClass = 'right';

const aboutPage = '/about';
const contactsPage = '/contacts';
const addItemPage = '/item/add';
const registrationPage = '/registration';
const loginPage = '/login';

export const newItemsPage = '/items/new';
export const popularItemsPage = '/items/popular';

export const Pages: Page[] = [
  {text: 'Главная', ref: '/', classes: [leftClass]},
  {
    text: 'Объявления',
    ref: 'javascript:void(0);',
    classes: ['dropdown', leftClass, 'dropbtn'],
    dropdowns: [
      {
        text: 'Новые Объявления',
        ref: newItemsPage,
        classes: [leftClass]
      },
      {
        text: 'Популярные объявления',
        ref: popularItemsPage,
        classes: [leftClass]
      }
    ]
  }, {
    text: 'О Нас',
    ref: aboutPage,
    classes: ['hide']
  }, {
    text: 'Политика конфиденциальности',
    ref: contactsPage,
    classes: ['hide']
  }, {
    text: 'Добавить Объявление',
    ref: addItemPage,
    classes: [leftClass],
    restriction: Restriction.User
  }, {
    text: 'Вход',
    ref: loginPage,
    classes: [rightClass],
    restriction: Restriction.Guest
  }, {
    text: 'Регистрация',
    ref: registrationPage,
    classes: [rightClass],
    restriction: Restriction.Guest
  }
];
