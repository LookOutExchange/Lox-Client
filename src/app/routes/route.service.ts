import {Page} from './iroute';
import {Injectable} from '@angular/core';
import {Pages, popularItemsPage, newItemsPage} from './route';

@Injectable()
export class RouteService {

  //  links and dropdowns on top menu
  private pages: Page[];
  // links on "pages" menu. Contains of pages variable
  private linkedPages: Page[];


  constructor() {
    this.pages = Pages;
    this.initLinkedPages();
  }

  private initLinkedPages() {
    const linkedPages: Array<Page> = [];
    this.pages.forEach((a) => {
      if (a.dropdowns != null) {
        a.dropdowns.forEach(b => {
          linkedPages.push(b);
        });
      } else {
        linkedPages.push(a);
      }
    });
    this.linkedPages = linkedPages;
  }

  getTopMenuPages(): Page[] {
    return this.pages;
  }

  getLinkedPages(): Page[] {
    return this.linkedPages;
  }

  getPopularItemsPage() {
    return popularItemsPage;
  }

  getNewItemsPage() {
    return newItemsPage;
  }

}
