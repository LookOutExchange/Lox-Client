import {Page} from './routes/iroute';
import {RouteService} from './routes/route.service';
import {Component, OnInit} from '@angular/core';
import {Category} from './item/item';
import {ItemService} from './item/item.service';
import {UserService} from './user/user.service';
import {User} from './user/user';
import {EventListener} from '@angular/core/src/debug/debug_node';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css'],
})
export class RootComponent implements OnInit {
  pages: Page[];
  linkedPage: Page[];
  categories?: Category[];
  user?: User;

  constructor(private itemService: ItemService, private routeService: RouteService, private userService: UserService) {
    this.user = userService.getFromLocalStorage();
    userService.userLogged$.subscribe(user => this.user = user);
  }

  ngOnInit() {
    this.pages = this.routeService.getTopMenuPages();
    this.linkedPage = this.routeService.getLinkedPages();
    this.itemService.getCategories().subscribe(data => this.categories = data);
  }

}
